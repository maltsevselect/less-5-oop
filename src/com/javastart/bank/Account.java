package com.javastart.bank;

import java.util.Date;

public class Account {

    private Person person;
    private Bill bill;
    private Date creationDate;

    public Account(Person person, Bill bill) {
        this.person = person;
        this.bill = bill;
        Date creationDate = new Date();
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Bill getBill() {
        return bill;
    }

    public void setBill(Bill bill) {
        this.bill = bill;
    }

    public Date getCreationDate() {
        return creationDate;
    }

}
