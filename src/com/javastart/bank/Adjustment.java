package com.javastart.bank;

import java.math.BigDecimal;
import java.util.Date;

public class Adjustment {

    private BigDecimal value;
    private Date creationDate;

    public Adjustment(BigDecimal value) {
        this.value = value;
        creationDate = new Date();
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public Date getCreationDate() {
        return creationDate;
    }
}
