package com.javastart.bank;

import com.javastart.bank.Services.AdjustmentService;
import com.javastart.bank.Services.PaymentService;
import com.javastart.bank.Services.TransferService;

import java.math.BigDecimal;

/**
 * В данном пакете нужно написать небольшую банковскую систему.
 * Минимальная банковская система будет состоять из сущностей: Bill (счет) Account (аккаунт)
 * Person (личность человека) Adjustment (корректировка счета) Payment (платеж).
 * Связи между сущностями будут такие: Account имеет ссылку на Person, Account имеет ссылку на Bill
 * Adjustment и Payment имеют ссылку на Bill
 * <p>
 * Помимо id, нужно будет добавить поля: например имя, фамилия, номер телефона к полю Person.
 * К Account можно добавить дату создания (необязательное условие)
 * Bill будет содеражать сумму счета, Payment и Adjustment будут содержать сумму платежа или корректировки
 * <p>
 * Один из сценариев должен быть таким: созадние нескольких аккаунтов и счетов, далее в сервисах совершение платежа (уменьшение счета)
 * и корректировка счета (увеличение счета)
 * <p>
 * Другой сценарий, перевод денег с одного счета на другой: отдельный сервис для такой операции, в методе перевода
 * <p>
 * Оперировать нужно сущностью Account и выводить в консоль совершение каждой каждой операции, фамилия имя клиента, состоянии на счету, имя операции
 * <p>
 * В методе main не должно происходить никакой логики, кроме вызовов сервисов.
 * Сервисы будут выполнять все действия, в методе main можно только создавать изначальные объекты и вызывать сервисы
 * <p>
 * Так же стоит предусмотреть критические случаи, например не оставлять отрицательную сумму на счету
 * <p>
 * Test commit
 */

public class Main {

    public static void main(String[] args) {


        Person maxim = new Person("Maxim", "Maltsev", "+7911");
        Person leonid = new Person("Leonid", "Tishkevich", "+7921");
        Person roma = new Person("Roman", "Pleshenko", "+7955");
        Bill bill1 = new Bill(new BigDecimal(1000));
        Bill bill2 = new Bill(new BigDecimal(2000));
        Bill bill3 = new Bill(new BigDecimal(3000));
        Bill bill4 = new Bill(new BigDecimal(500));
        Bill bill5 = new Bill(new BigDecimal(100));
        Account accountMaxim = new Account(maxim, bill1);
        Account accountLeonid = new Account(leonid, bill2);
        Account accountRoma = new Account(roma, bill3);



        //Сценарий 1 - оплата со счета
        PaymentService.makePayment(accountMaxim, new Payment(bill4));
        PaymentService.makePayment(accountMaxim, new Payment(bill5));
        PaymentService.makePayment(accountMaxim, new Payment(bill4)); //недостаточно денег на счету, платеж не пройдет
        PaymentService.makePayment(accountMaxim, new Payment(bill5)); //для этого - достаточно, платеж пройдет

        AdjustmentService adjustmentService = new AdjustmentService();
        adjustmentService.makeAdjustment(accountRoma, new Adjustment(new BigDecimal(50)));
        adjustmentService.makeAdjustment(accountRoma, new Adjustment(new BigDecimal(100)));
        adjustmentService.makeAdjustment(accountMaxim, new Adjustment(new BigDecimal(10500)));

        //Сценарий2 - переводы с аккаунта на аккаунт
        TransferService transferService = new TransferService();
        transferService.makeTransfer(accountLeonid, accountMaxim, new BigDecimal(1999));
        transferService.makeTransfer(accountLeonid, accountMaxim, new BigDecimal(500)); //Столько уже не хватит


    }
}
