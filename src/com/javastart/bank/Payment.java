package com.javastart.bank;

import java.util.Date;

public class Payment {

    private Bill bill;
    private Date creationDate;

    public Payment(Bill bill) {
        this.bill = bill;
        creationDate = new Date();
    }

    public Bill getBill() {
        return bill;
    }

    public void setBill(Bill bill) {
        this.bill = bill;
    }

    public Date getCreationDate() {
        return creationDate;
    }
}
