package com.javastart.bank;

public class Person {

    private long id;
    private String name;
    private String lastname;
    private String phone;

    public Person(String name, String lastname, String phone) {
        this.id = PersonSequenceGenerator.getCount();
        this.name = name;
        this.lastname = lastname;
        this.phone = phone;
        //System.out.println("Создан Person " + this.name + ", id = " + this.id);
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
