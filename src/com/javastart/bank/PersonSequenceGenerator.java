package com.javastart.bank;

import java.util.concurrent.atomic.AtomicLong;

public class PersonSequenceGenerator {

    private static AtomicLong count = new AtomicLong(0);

    public static long getCount() {
        return count.incrementAndGet();
    }
}
