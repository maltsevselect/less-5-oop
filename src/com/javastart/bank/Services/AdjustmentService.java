package com.javastart.bank.Services;

import com.javastart.bank.Account;
import com.javastart.bank.Adjustment;

public class AdjustmentService {

    public static void makeAdjustment(Account account, Adjustment adjustment) {
        account.getBill().setAmount(account.getBill().getAmount().add(adjustment.getValue()));
        System.out.println("Был произведен Adjustment на аккаунт, принадлежащий " + account.getPerson().getName() + " "
                + account.getPerson().getLastname() + " , добавлена сумма " + adjustment.getValue().toString()
                + ", новый баланс = " + account.getBill().getAmount() + ". Дата операции:"
                + adjustment.getCreationDate());
    }
}
