package com.javastart.bank.Services;

import com.javastart.bank.Account;
import com.javastart.bank.Bill;
import com.javastart.bank.Payment;

public class PaymentService {

    public static void makePayment(Account account, Payment payment) {
        Bill accountBill = account.getBill();
        Bill paymentBill = payment.getBill();

        if (accountBill.getAmount().compareTo(payment.getBill().getAmount()) >= 0) {
            accountBill.setAmount(accountBill.getAmount().subtract(paymentBill.getAmount()));
            System.out.println(account.getPerson().getName() + " " + account.getPerson().getLastname() + " произвел платеж размером "
                    + paymentBill.getAmount().toString() + ", новая сумма на счету:" + accountBill.getAmount().toString()
                    + ". Дата операции:" + payment.getCreationDate());
        } else {
            System.out.println("Недостаточно денег для проведения платежа на сумму " + paymentBill.getAmount().toString());
        }
    }
}
