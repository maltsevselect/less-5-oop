package com.javastart.bank.Services;

import com.javastart.bank.Account;
import com.javastart.bank.Bill;
import com.javastart.bank.Person;

import java.math.BigDecimal;
import java.util.Date;

public class TransferService {

    public static void makeTransfer(Account fromAccount, Account toAccount, BigDecimal amountBigDecimal) {

        Bill fromBill = fromAccount.getBill();
        Bill toBill = toAccount.getBill();
        Person fromPerson = fromAccount.getPerson();
        Person toPerson = toAccount.getPerson();

        if (fromBill.getAmount().compareTo(amountBigDecimal) >= 0) {
            fromBill.setAmount(fromBill.getAmount().subtract(amountBigDecimal));
            toBill.setAmount(toBill.getAmount().add(amountBigDecimal));
            System.out.println("Совершен платеж от " + fromPerson.getName() + " " + fromPerson.getLastname() + " в пользу " +
                    toPerson.getName() + " " + toPerson.getLastname() + " на сумму " + amountBigDecimal.toString() +
                    ", остаток у отправителя = " + fromBill.getAmount().toString() +
                    ", остаток у получателя = " + toBill.getAmount().toString() +
                    ". Дата операции: " + new Date());
        } else {
            System.out.println("Для платежа недостаточно средств. На счету отправителя " + fromBill.getAmount().toString()
                    + ", а требуется " + amountBigDecimal.toString());
        }
    }
}
