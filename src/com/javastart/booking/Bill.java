package com.javastart.booking;

import java.math.BigDecimal;

public class Bill {

    private BigDecimal amount;

    public Bill(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
