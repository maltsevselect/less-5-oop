package com.javastart.booking;

import com.javastart.booking.SequenceGenerators.GuestSequenceGenerator;

public class Guest {

    private long id;
    private String name;
    private String lastname;
    private String phone;
    private String email;
    private Bill bill;
    private Room orderedRoom;

    public Guest(String name, String lastname, String phone, String email, Bill bill) {
        this.id = GuestSequenceGenerator.getCount();
        this.name = name;
        this.lastname = lastname;
        this.phone = phone;
        this.email = email;
        this.bill = bill;
        this.orderedRoom = null;
    }

    public Guest(String name, String lastname, String phone, Bill bill) {
        this.id = GuestSequenceGenerator.getCount();
        this.name = name;
        this.lastname = lastname;
        this.phone = phone;
        this.bill = bill;
        this.orderedRoom = null;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getLastname() {
        return lastname;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }

    public Bill getBill() {
        return bill;
    }

    public Room getOrderedRoom() {
        return orderedRoom;
    }

    public void setOrderedRoom(Room orderedRoom) {
        this.orderedRoom = orderedRoom;
    }
}
