package com.javastart.booking;

public class Hotel {

    private String name;
    private Room[] rooms;
    private int countFreeRooms;
    private boolean hasFreeRooms;

    public Hotel(String name, Room[] rooms) {
        this.name = name;
        this.rooms = rooms;
        calculateFreeRooms(); //Ничего, что при создании считаются свободные комнаты и выставляется boolean-флаг?
        fillHotelToRooms(); //Чтобы каждая комната понимала в каком отеле находится
                            // (чтобы можно было ссылаться из комнаты на отель)
    }

    private void fillHotelToRooms() {
        for (Room room: rooms) {
            room.setHotel(this);
        }
    }

    public void calculateFreeRooms() {
        int count = 0;
        for (Room room : rooms) {
            if (!room.isBooked()) {
                count++;
            }
        }
        countFreeRooms = count;
        hasFreeRooms = count > 0; //Гениально - да?
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Room[] getRooms() {
        return rooms;
    }

    public void setRooms(Room[] rooms) {
        this.rooms = rooms;
    }

    public boolean hasFreeRooms() {
        return hasFreeRooms;
    }

    public int getCountFreeRooms() {
        return countFreeRooms;
    }

    public void setCountFreeRooms(int countFreeRooms) {
        this.countFreeRooms = countFreeRooms;
    }

    public boolean isHasFreeRooms() {
        return hasFreeRooms;
    }

    public void setHasFreeRooms(boolean hasFreeRooms) {
        this.hasFreeRooms = hasFreeRooms;
    }
}
