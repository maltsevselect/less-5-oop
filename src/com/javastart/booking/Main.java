package com.javastart.booking;

import com.javastart.booking.Services.BookingService;
import com.javastart.booking.Services.CancelBookingService;
import com.javastart.booking.Services.FindRoomService;

import java.math.BigDecimal;

/**
 * В данном пакете нужно написать систему бронировния номеров.
 * Сущности: Client, Hotel, Room, Bill.
 * <p>
 * Client будет содержать поля (кроме id) имя, фамилию, номер телефона, почту, ссылку на Bill
 * В Bill будет сумма клиента
 * Hotel будет содеражть имя отеля, список свободных номеров, общий список номеров, логическую переменную есть ли свободные номера
 * Room будет содеражть колличество человек, которых можно разместить и стоимость этого номера
 * <p>
 * Нужно создать нескольких клиентов с счетами и внести им первоначальную сумму. Так же нужно создать
 * несколько отелей, с разными наборами комнат (В списке внутри сущности Hotel)
 * <p>
 * При бронировании комнаты, нужно ее удалять из списка комнат в отеле и проверять если что-то в этом списке, если нет,
 * выставлять переменную boolean свободных номеров как false (нужно написать такой кейс)
 * <p>
 * Так же при бронировании проверять есть ли свободные номера (нужно написать такой кейс)
 * <p>
 * В одном из кейсов, клиенту должно не хватить денег для бронирования номера
 * <p>
 * После бронировния списывать сумму со счета клиента
 * <p>
 * В методе main не нужно проводить никаких операций, кроме создания начальных объектов и сервисов, так же можно вызывать методы сервисов
 * Каждое действие, с бронированием, списыванием со счета и тд. нужно выводить в консоль
 */

public class Main {

    public static void main(String[] args) {

        Guest maxim = new Guest("Maxim", "Maltsev", "+7911208", "maxim1801@mail.ru", new Bill(new BigDecimal(5000)));
        Guest leonid = new Guest("Leonid", "Tishkevich", "+7912", new Bill(new BigDecimal(15000)));
        Guest roman = new Guest("Roman", "Pleshchenko", "+7915", "romkamolodec@yandex.ru", new Bill(new BigDecimal(4000)));

        Room[] superDuperRooms = {new Room(5, new BigDecimal(1000)),
                new Room(3, new BigDecimal(600)),
                new Room(2, new BigDecimal(900)),
                new Room(2, new BigDecimal(2000))};
        Room[] helloWorldRooms = {new Room(1, new BigDecimal(1000)),
                new Room(9, new BigDecimal(6000)),
                new Room(4, new BigDecimal(900)),
                new Room(2, new BigDecimal(2000))};
        Room[] javaSchoolRooms = {new Room(3, new BigDecimal(5000)),
                new Room(2, new BigDecimal(6000)),
                new Room(6, new BigDecimal(7000)),
                new Room(2, new BigDecimal(8000))};

        Hotel superDuper = new Hotel("SuperDuper", superDuperRooms);
        Hotel helloWorld = new Hotel("HelloWorld", helloWorldRooms);
        Hotel javaSchool = new Hotel("JavaSchool", javaSchoolRooms);

        FindRoomService findRoomService = new FindRoomService();

        findRoomService.findAndBookRoomInHotel(maxim, superDuper, 2); //Успешное бронирование 1ой комнаты
        findRoomService.findAndBookRoomInHotel(leonid, superDuper, 2);//Успешное бронирование 2ой комнаты, потому что 1ая уже занята
        findRoomService.findAndBookRoomInHotel(roman, javaSchool, 2); //Роме не по карману ни одна комната в JavaSchool (javaSchool)
        findRoomService.findAndBookRoomInHotel(roman, helloWorld, 2); //Успешное бронирование для Ромы во втором отеле
        findRoomService.findAndBookRoomInHotel(roman, superDuper, 2); //Отказ Роме в бронировании, т.к. на него уже забронирован номер в одном из отелей

        CancelBookingService.cancelReservationFromGuest(roman); //Выселим рому из номера
        findRoomService.findAndBookRoomInHotel(roman, superDuper, 2); //успешно забронирована 3я комната, потому что 1 и 2 заняты

        CancelBookingService.cancelReservationHotel(superDuper); // Выселим всех из отеля superDuper


    }
}
