package com.javastart.booking;

import com.javastart.booking.SequenceGenerators.RoomSequenceGenerator;

import java.math.BigDecimal;

public class Room {

    private long id;
    private int numberOfPlaces;
    private BigDecimal cost;
    private boolean booked;
    private Guest guest;
    private Hotel hotel;

    public Room(int numberOfPlaces, BigDecimal cost) {
        this.numberOfPlaces = numberOfPlaces;
        this.cost = cost;
        this.booked = false;
        this.id = RoomSequenceGenerator.getCount();
    }

    public void setNumberOfPlaces(int numberOfPlaces) {
        this.numberOfPlaces = numberOfPlaces;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public int getNumberOfPlaces() {
        return numberOfPlaces;
    }

    public boolean isBooked() {
        return booked;
    }

    public void setBooked(boolean booked) {
        this.booked = booked;
    }

    public Guest getGuest() {
        return guest;
    }

    public void setGuest(Guest guest) {
        this.guest = guest;
    }

    public long getId() {
        return id;
    }

    public Hotel getHotel() {
        return hotel;
    }

    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }
}
