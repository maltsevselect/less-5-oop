package com.javastart.booking.SequenceGenerators;

import java.util.concurrent.atomic.AtomicLong;

public class RoomSequenceGenerator {

    private static AtomicLong count = new AtomicLong(0);

    public static long getCount(){
        return count.incrementAndGet();
    }
}
