package com.javastart.booking.Services;


import com.javastart.booking.Guest;
import com.javastart.booking.Hotel;
import com.javastart.booking.Room;

import java.math.BigDecimal;

public class BookingService {

    public static void booking(Guest guest, Hotel hotel, Room roomForBooking) {
        BigDecimal amount = guest.getBill().getAmount();
        if (amount.compareTo(roomForBooking.getCost()) >= 0) { //Дублирование проверки платежеспособности, "для надежности" ;)
            guest.getBill().setAmount(amount.subtract(roomForBooking.getCost())); //вычитаем стоимость номера со счета гостя
            for (Room currentRoom : hotel.getRooms()) {
                if (currentRoom.equals(roomForBooking)) {
                    currentRoom.setBooked(true);
                    currentRoom.setGuest(guest);
                    guest.setOrderedRoom(currentRoom);
                    hotel.calculateFreeRooms();
                    System.out.println("Для " + guest.getName() + " " + guest.getLastname() + " забронирован номер в отеле "
                            + hotel.getName() + " вместимостью " + currentRoom.getNumberOfPlaces() + ". Номер комнаты "
                            + currentRoom.getId() + ". Со счета списано " + currentRoom.getCost()
                            + ", остаток на счету гостя: " + guest.getBill().getAmount().toString());
                    break;
                }
            }
        }
    }
}
