package com.javastart.booking.Services;

import com.javastart.booking.Guest;
import com.javastart.booking.Hotel;
import com.javastart.booking.Room;

public class CancelBookingService {
    public static void cancelReservationFromGuest(Guest guest) {
        if (guest.getOrderedRoom() != null) {
            Room room = guest.getOrderedRoom();
            Hotel hotel = room.getHotel();
            System.out.println("Из отеля " + hotel.getName() + " персонально выселен " + room.getGuest().getName() + " "
                    + room.getGuest().getLastname() + ". Он проживал в номере " + room.getId());
            room.setBooked(false);
            hotel.calculateFreeRooms();
            guest.setOrderedRoom(null);
        }

    }

    public static void cancelReservationHotel(Hotel hotel) {
        for (Room room : hotel.getRooms()) {
            if (room.getGuest() != null) {
                System.out.println("Из отеля " + hotel.getName() + " массово выселен " + room.getGuest().getName() + " "
                        + room.getGuest().getLastname() + ". Он проживал в номере " + room.getId());
                room.getGuest().setOrderedRoom(null);
                room.setBooked(false);
                hotel.calculateFreeRooms();
            }
        }
    }
}
