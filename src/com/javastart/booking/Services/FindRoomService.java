package com.javastart.booking.Services;

import com.javastart.booking.Guest;
import com.javastart.booking.Hotel;
import com.javastart.booking.Room;

public class FindRoomService {

    private BookingService bookingService = new BookingService();

    public void findAndBookRoomInHotel(Guest guest, Hotel hotel, int places) {
        boolean isFreeRoomFoundWithPlaces = false;
        boolean isRoomBooked = false;

        if (guest.getOrderedRoom() == null) {
            continueSearchingGuestDoesntHaveRoom(hotel, guest, places, isFreeRoomFoundWithPlaces, isRoomBooked);
        } else {
            printMessageDoubleBooking(guest);
        }
    }

    private void continueSearchingGuestDoesntHaveRoom(Hotel hotel, Guest guest, int places, boolean isFreeRoomFoundWithPlaces, boolean isRoomBooked) {
        if (hotel.hasFreeRooms()) {
            continueSearchingHotelHasFreeRooms(hotel, guest, places, isFreeRoomFoundWithPlaces, isRoomBooked);
        } else {
            printMessageDontHaveFreeRooms(guest);
        }
    }

    private void continueSearchingHotelHasFreeRooms(Hotel hotel, Guest guest, int places, boolean isFreeRoomFoundWithPlaces, boolean isRoomBooked) {
        Room[] allRooms = hotel.getRooms();
        for (Room room : allRooms) {
            if (!room.isBooked() && room.getNumberOfPlaces() >= places) {
                isFreeRoomFoundWithPlaces = true;
                if (guest.getBill().getAmount().compareTo(room.getCost()) >= 0) {
                    bookingService.booking(guest, hotel, room);
                    isRoomBooked = true;
                    break;
                }

            }
        }
        if (!isFreeRoomFoundWithPlaces) {
            printMessageNotFoundRoom(guest, places);
        } else {
            if (!isRoomBooked) {
                printMessageNotEnoughMoney(guest);
            }
        }
    }

    private static void printMessageNotFoundRoom(Guest guest, int places) {
        System.out.println("Для " + guest.getName() + " " + guest.getLastname() + " подходящих свободных номеров на "
                + places + " места(мест) не найдено.");
    }

    private static void printMessageNotEnoughMoney(Guest guest) {
        System.out.println("Произошла ошибка. Вероятно, " + guest.getName() + " " + guest.getLastname()
                + " не может себе позволить ни один из подходящих номеров. Сожалеем.");
    }

    private static void printMessageDontHaveFreeRooms(Guest guest) {
        System.out.println("Бронирование для " + guest.getName() + " не произведено. Свободных номеров нет.");
    }

    private static void printMessageDoubleBooking(Guest guest) {
        System.out.println("У гостя " + guest.getName() + " " + guest.getLastname() + "уже забронирован номер. Два номера одному человеку ни к чему ;)");
    }
}
